#include <LightField.hpp>

LightField::LightField(float xpos)
  : SpaceObject()
{
  m_shape.setSize({LightfieldWidth, LightfieldHeight});
  m_shape.setFillColor(sf::Color::White);
  m_shape.setTexture(&Core::instance()->assets().texture("lightfields"));
  
  m_dir = {0.0f, -1.0, 0.0};
  m_pos = {xpos, -m_shape.getSize().y, 0.0f};
  m_vel = {0.0f, m_speed, 0.0f};
}



LightField::~LightField()
{
  
}
