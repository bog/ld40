#include <Player.hpp>
#include <Particules.hpp>

Player::Player()
  : m_escape (false)
{
  m_shape.setSize({PlayerWidth, PlayerHeight});
  
  sf::Vector2f winsz {
    static_cast<float>(Core::instance()->window().getSize().x),
      static_cast<float>(Core::instance()->window().getSize().y)
      };
  
  const float offset = 2;
  
  m_pos = {
    (winsz.x - m_shape.getSize().x)/2,
    winsz.y - offset * m_shape.getSize().y,
    0 };

  m_vel = {0, 0, 0};
  m_acc = {0, 0, 0};
  m_dir = {0, -1, 0};
  m_friction = 0.98f;
  
  m_shape.setFillColor(sf::Color::White);
  m_shape.setTexture(&Core::instance()->assets().texture("player"));

  m_particules = std::make_unique<Particules>();
  m_pcolor = sf::Color::Cyan;
}

void Player::update(float dt)
{
  // Inputs
  int const hspeed = 1024;

  m_dir = {0, -1, 0};

  if (!m_escape)
    {
      if ( sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Left) )
	{
	  m_acc.x -= hspeed;
	  m_dir = glm::normalize(glm::vec3{-1.0f, -1.0f, 0.0f});
	}

      if ( sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Right) )
	{
	  m_acc.x += hspeed;
	  m_dir = glm::normalize(glm::vec3{1.0f, -1.0f, 0.0f});
	}
    }
  else
    {
      m_acc.y -= hspeed*4;
      m_dir = glm::vec3{0.0f, -1.0f, 0.0f};
    }
  
  m_vel = m_acc * dt;
  Actor::update(dt);
  
  // horizontale friction
  if (m_dir.x == 0)
    {
      m_acc *= glm::vec3 {m_friction, 1.0f, 1.0f};
    }

  // Wall collisions
  if (m_pos.x < 0)
    {
      m_pos.x = 0;
    }

  if (m_pos.x + m_shape.getSize().x > Core::instance()->window().getSize().x)
    {
      m_pos.x = Core::instance()->window().getSize().x - m_shape.getSize().x;
    }

  // Update shape
  m_shape.setPosition({m_pos.x, m_pos.y});


  static float timer = 0;
  
  if (timer > 0.2)
    {
      float ph = 4;
      m_particules->fire(64,
			 m_pcolor,
			 ph,
			 glm::vec3(pos().x + (PlayerWidth - ph)/2,
				   pos().y + 0.9f * PlayerHeight ,
				   0.0f),
			 -dir(),
			 0.1f,
			 2.0f,
			 512.0f);

      timer = 0;
    }

  timer += dt;
  
  m_particules->update(dt);
}

void Player::display(sf::RenderWindow& w)
{
  Actor::display(w);
  m_particules->display(w);
}

Player::~Player()
{
  
}
