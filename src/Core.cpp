#include <Core.hpp>
// Scenes
#include <LevelScene.hpp>
#include <BgScene.hpp>
//

Core* Core::m_instance = nullptr;

Core::Core()
{
  // do not create services here. Wait for run function.
}

void Core::update(float dt)
{
  if (m_scene)
    {
      m_scene->update(dt);
    }
}

void Core::display(sf::RenderWindow& window)
{
  if (m_scene)
    {
      m_scene->display(window);
    }
}

void Core::run()
{
  // Service
  m_assets = std::make_unique<Assets>();
  m_assets->loadTexture("asteroids", ASSETS + "/sprites/asteroids.png");
  m_assets->loadTexture("lightfields", ASSETS + "/sprites/lightfields.png");
  m_assets->loadTexture("player", ASSETS + "/sprites/player.png");
  m_assets->loadTexture("menu_scene", ASSETS + "/sprites/menu_scene.png");
  m_assets->loadTexture("gameover_scene", ASSETS + "/sprites/gameover_scene.png");
  m_assets->loadTexture("gamecomplete_scene", ASSETS + "/sprites/gamecomplete_scene.png");
  m_assets->loadTexture("help_scene", ASSETS + "/sprites/help_scene.png");
  m_assets->loadTexture("sky", ASSETS + "/sprites/bg.png");
  m_assets->loadTexture("particule", ASSETS + "/sprites/particule.png");
  m_assets->loadTexture("alarm_feedback", ASSETS + "/sprites/alarm_feedback.png");
  m_assets->loadMusic("menu_music", ASSETS + "/musics/home_space_home.ogg");
  m_assets->loadMusic("level_music", ASSETS + "/musics/where_there_s_no_light.ogg");
  m_assets->loadMusic("help_music", ASSETS + "/musics/a_game_of_rocks.ogg");
  m_assets->loadMusic("gameover_music", ASSETS + "/musics/eternity.ogg");
  m_assets->loadMusic("gamecomplete_music", ASSETS + "/musics/survivors.ogg");
  m_assets->loadSBuffer("asteroid_entry", ASSETS + "/sounds/asteroid_entry.ogg");
  m_assets->loadSBuffer("light_entry", ASSETS + "/sounds/light.ogg");
  m_assets->loadSBuffer("alarm_entry", ASSETS + "/sounds/alarm.ogg");
  
  m_music = nullptr;
  m_sound_index = 0;
  //

  
  scene(new MenuScene);  
  
  
  // Core loop
  sf::Event event;
  sf::Clock clock;
  float dt = 0;
  
  while ( m_window->isOpen() )
    {      
      clock.restart();
      
      while ( m_window->pollEvent(event) )
	{
	  switch (event.type)
	    {
	      case sf::Event::Closed:
		m_window->close();
		break;
	      default:;
	    }
	}

      update(dt);
      
      m_window->clear();
      display(*m_window);
      m_window->display();

      dt = clock.getElapsedTime().asSeconds();
    }
}

void Core::scene()
{
  scene(new MenuScene);
}

void Core::scene(Scene* s)
{
  m_scene = std::unique_ptr<Scene>(s);
}

void Core::stopMusic()
{
  if (m_music != nullptr) { m_music->stop(); }
}

void Core::playMusic(std::string name)
{
  playMusic(name, 100.0f);
}

void Core::playMusic(std::string name, float volume)
{
  stopMusic();
  
  m_music = &m_assets->music(name);
  m_music->setVolume(volume);
  m_music->play();
}

void Core::playSound(std::string name)
{
  m_sound[m_sound_index].setBuffer(m_assets->sbuffer(name));
  m_sound[m_sound_index].play();

  m_sound_index++;
  if (m_sound_index >= SOUND_LIMIT)
    {
      m_sound_index = 0;
    }
}

Core::~Core()
{
  m_scene = nullptr;
}
