#include <SpaceObject.hpp>

SpaceObject::SpaceObject()
  : m_falling (true)
  , m_speed (128)
{
  m_shape.setSize({32, 32});
  m_shape.setFillColor(sf::Color::Red);
  
  m_dir = {0.0f, -1.0, 0.0};
  m_pos = {0.0f, - m_shape.getSize().y, 0.0f};
  m_vel = {0.0f, m_speed, 0.0f};
}

void SpaceObject::update(float dt)
{
  if (!m_falling) { return; }
  m_vel = {0.0f, m_speed, 0.0f};
  Actor::update(dt);  
  m_shape.setPosition({m_pos.x, m_pos.y});

  if (m_pos.y > Core::instance()->window().getSize().y)
    {
      m_falling = false;
    }
}


SpaceObject::~SpaceObject()
{
  
}
