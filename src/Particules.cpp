#include <cassert>
#include <chrono>
#include <random>
#include <Particules.hpp>

Particules::Particules()
  : m_size (0)
{
  assert(m_size <= PARTICULES_MAX);
  m_shape.setTexture(&Core::instance()->assets().texture("particule"));

}

void Particules::fire(size_t size,
		      sf::Color color,
		      float rad,
		      glm::vec3 origin,
		      glm::vec3 dir,
		      float spread,
		      float life,
		      float speed)
{
  m_size = size;
  m_shape.setFillColor(color);
  m_shape.setSize({rad, rad});
  
  std::mt19937 rand (std::chrono::steady_clock::now().time_since_epoch().count());
  std::uniform_real_distribution<float> urds (-spread, spread);
      
  for (size_t i=0; i<m_size; i++)
    {
      m_pool[i].life = life;
      m_pool[i].pos = origin;
      m_pool[i].dir = glm::normalize(dir + glm::vec3{urds(rand), urds(rand), 0});
      m_pool[i].speed = speed;
    }
}

void Particules::update(float dt)
{
  for (size_t i=0; i<m_size; i++)
    {
      if (m_pool[i].alive)
	{
	  m_pool[i].pos += m_pool[i].dir * m_pool[i].speed * dt;
	  m_pool[i].life -= dt;

	  if (m_pool[i].life <= 0)
	    {
	      m_pool[i].alive = false;
	      m_size--;
	      m_pool[i] = m_pool[m_size];
	      i--;
	    }
	}
      else
	{
	  m_size--;
	  m_pool[i] = m_pool[m_size];
	  i--;
	}

    }

}

void Particules::display(sf::RenderWindow& w)
{
  for (size_t i=0; i<m_size; i++)
    {
      if (m_pool[i].alive)
	{
	  m_shape.setPosition(sf::Vector2f {m_pool[i].pos.x, m_pool[i].pos.y});
	  w.draw(m_shape);
	}
    }
}

Particules::~Particules()
{
  
}
