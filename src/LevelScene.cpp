#include <chrono>
#include <random>
#include <LevelScene.hpp>
#include <BgScene.hpp>
#include <ProgressBar.hpp>
#include <Sky.hpp>
#include <Particules.hpp>
// Actor
#include <Player.hpp>
#include <Asteroid.hpp>
#include <LightField.hpp>

LevelScene::LevelScene()
  : m_state (LevelState::Playing)
  , m_alarm_enabled (false)
  , m_player_explodes (false)
  , m_speed_gain (1.0f/16)
  , m_ui_xratio (0.05)
  , m_asteroids_freq (0.2)
  , m_lightfields_freq (0.8)
  , m_boost_freq (0.0025)
  , m_asteroids_speed (300)
  , m_lightfields_speed (m_asteroids_speed)
  , m_boost_speed (1.1f)
  , m_boost_count (0)
{
  m_player = std::make_unique<Player>();

  m_sky = std::make_unique<Sky>();
  m_particules = std::make_unique<Particules>();  
  
  sf::Vector2f winsz = {
    static_cast<float>( Core::instance()->window().getSize().x ),
    static_cast<float>( Core::instance()->window().getSize().y )
  };

  m_bg.setSize(winsz);
  m_bg.setTexture(&Core::instance()->assets().texture("sky"));
  m_alarm_feedback.setSize(winsz);
  m_alarm_feedback.setTexture(&Core::instance()->assets().texture("alarm_feedback"));
  
  Core::instance()->playMusic("level_music");
  m_music = &Core::instance()->assets().music("level_music");
}

void LevelScene::update(float dt)
{  
  // Inputs
  if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Escape))
    {
      Core::instance()->scene(new MenuScene());
      return;
    }

  
  if (m_state != LevelState::Playing)
    {
      updateAnimation(dt);
      return;
    }

  if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Space))
    {
      if (m_speed_bar->value() >= 1.0)
	{
	  gameComplete();
	}
    }
  
  m_player->update(dt);
  
  updateAsteroids(dt);  
  m_sky->speed(m_asteroids_speed * 1.025);
  updateLightfields(dt);

  updateAsteroidsGeneration(dt);
  updateLightfieldsGeneration(dt);
  updateUI(dt);
  
  m_sky->update(dt);
  m_particules->update(dt);

  updateAlarm(dt);
}

void LevelScene::updateAnimation(float dt)
{
  // Waiting animation
  static float timer = 0;
  static const float anim_time = 5.0;

  float volume = 100.0f * ( 1 - (timer/anim_time) );
  
  m_music->setVolume(volume);
  
  if (timer > anim_time)
    {
      if (m_state == LevelState::GameOver)
	{
	  timer = 0;
	  Core::instance()->scene(new GameOverScene());
	  return;
	}
      else if (m_state == LevelState::GameComplete)
	{
	  timer = 0;
	  Core::instance()->scene(new GameCompleteScene());
	  return;
	}
      else
	{
	  std::cerr<< "Game error" <<std::endl;
	  exit(-1); // please no fall here :(
	}      
    }
  
  timer += dt;

  // Keep the move effect
  m_sky->update(dt);
  updateAsteroids(dt);
  updateLightfields(dt);
  
  // Choosing animation
  if (m_state == LevelState::GameOver)
    {
      updateAnimationGameOver(dt);
    }
  else if (m_state == LevelState::GameComplete)
    {
      updateAnimationGameComplete(dt);
    }      
}

void LevelScene::updateAnimationGameOver(float dt)
{
  if (!m_player_explodes)
    {
      m_particules->fire(32,
			 sf::Color::Yellow,
			 8,
			 m_player->pos(),			     
			 glm::vec3{0, 0, 0},
			 256.0,
			 5,
			 512.0);
      
      m_player_explodes = true;
    }
  
  m_particules->update(dt);
}

void LevelScene::updateAnimationGameComplete(float dt)
{
  m_player->escape(true);
  m_player->update(dt);
}

void LevelScene::updateAsteroids(float dt)
{
  // Remove hidden asteroids
  m_asteroids.erase( std::remove_if(m_asteroids.begin(), m_asteroids.end(),
				    [](std::unique_ptr<Asteroid>& asteroid){
				      return !asteroid->falling();				      
				    }),
		     m_asteroids.end() );
  
  // Update asteroids
  for (auto& asteroid : m_asteroids)
    {
      asteroid->update(dt);
      asteroid->speed(m_asteroids_speed);
      
      // Collision with player
      if (m_state == LevelState::Playing
	  && asteroid->hitbox().intersects(m_player->hitbox()))
	{
	  Core::instance()->playSound("asteroid_entry");
	  
	  m_particules->fire(16,
	  		     sf::Color{150, 150, 150},
	  		     16,
	  		     m_player->pos(),			     
	  		     glm::vec3{0, 0, 0},
	  		     256.0,
	  		     0.5,
	  		     256.0);
	  
	  boostDown();
	  asteroid->falling(false);
	}

      for (auto& asteroid2 : m_asteroids)
	{
	  // Collision with others
	  if (asteroid->hitbox().intersects(asteroid2->hitbox())
	      && asteroid != asteroid2)
	    {
	      asteroid->falling(false);
	    }      
	}
    }
}

void LevelScene::updateLightfields(float dt)
{
  // Remove hidden lightfields
  m_lightfields.erase( std::remove_if(m_lightfields.begin(), m_lightfields.end(),
				      [](std::unique_ptr<LightField>& lightfield){
					return !lightfield->falling();				      
				      }),
		       m_lightfields.end() );
  
  // Update lightfields
  for (auto& lightfield : m_lightfields)
    {
      lightfield->update(dt);
      lightfield->speed(m_lightfields_speed);
      
      // Collision with player
      if (m_state == LevelState::Playing
	  && lightfield->hitbox().intersects(m_player->hitbox()))
	{
	  Core::instance()->playSound("light_entry");
	  
	  m_particules->fire(50,
	  		     sf::Color{0, 255, 255, 16},
	  		     16,
	  		     glm::vec3(m_player->pos().x + PlayerWidth/2,
	  			       m_player->pos().y + PlayerHeight/2,
	  			       0),
	  		     -lightfield->dir(),
	  		     0.4,
	  		     1.0,
	  		     512.0);
	  
	  boostUp();
	  lightfield->falling(false);
	}
    }
}

void LevelScene::updateAsteroidsGeneration(float dt)
{
  static float timer = 0;
  
  timer += dt;

  std::mt19937 randposx ( std::chrono::steady_clock::now().time_since_epoch().count() );
  std::uniform_real_distribution<float> dist (0, Core::instance()->window().getSize().x
					      - AsteroidSize);
  float nextx = dist(randposx);

  if (timer > m_asteroids_freq)
    {
      m_asteroids.push_back(std::make_unique<Asteroid>(nextx));
      
      timer = 0;
    }
}

void LevelScene::updateLightfieldsGeneration(float dt)
{
  static float timer = 0;
  
  timer += dt;

  std::mt19937 randposx ( std::chrono::steady_clock::now().time_since_epoch().count() );
  std::uniform_real_distribution<float> dist (0, Core::instance()->window().getSize().x
					      - LightfieldWidth);
  float nextx = dist(randposx);

  if (timer > m_lightfields_freq)
    {
      m_lightfields.push_back(std::make_unique<LightField>(nextx));
      timer = 0;
    }
}


void LevelScene::updateUI(float dt)
{
  static float timer = 0.0f;
  static const float freq = 0.2;
  
  if (m_speed_bar)
    {
      if ( m_speed_bar->value() >= 1 )
	{
	  if (timer >= freq)
	    {	      
	      timer = 0.0f;
	    }
	  float a = 0.5 * ( 1 + sin(timer/freq) );
	  
	  sf::Color col = {
	    0,
	    255,
	    255,
	    static_cast<sf::Uint8>(255 * a)
	  };
	  
	  m_speed_bar->valueColor(col);
	}
      else
	{
	  m_speed_bar->valueColor(sf::Color::Cyan);
	  timer = 0.0f;
	}
      
      m_speed_bar->update(dt);
    }
  
  if (m_life_bar)
    {
      m_life_bar->update(dt);
    }

  timer += dt;
}

void LevelScene::updateAlarm(float dt)
{
  static float timer = 0;
  static const float freq = 1.0;
  static const float critic = 0.5;
  
  if (m_life_bar->value() >= 0.5)
    {
      timer = 0;
    }  
  else if (m_life_bar->value() < 0.5 && m_life_bar->value() > 0.3)
    {
      float a = timer/freq;
      a = abs(sin(a))/2.0;
      
      m_alarm_feedback.setFillColor(sf::Color{
	  m_alarm_feedback.getFillColor().r,
	    m_alarm_feedback.getFillColor().g,
	    m_alarm_feedback.getFillColor().b,
	    static_cast<sf::Uint8>(a * 255.0f)
	    });
      
      if (timer > freq)
	{
	  Core::instance()->playSound("alarm_entry");
	  m_player->pcolor(sf::Color::Yellow);
	  m_alarm_enabled = true;
	  timer = 0;
	}
    }
  else if (m_life_bar->value() <= 0.3)
    {
      float a = timer/(critic * freq);
      a = abs(sin(a))/2.0;
      
      m_alarm_feedback.setFillColor(sf::Color{
	  m_alarm_feedback.getFillColor().r,
	    m_alarm_feedback.getFillColor().g,
	    m_alarm_feedback.getFillColor().b,
	    static_cast<sf::Uint8>(a * 255.0f)
	    });

      if (timer > critic * freq)
	{
	  Core::instance()->playSound("alarm_entry");
	  m_player->pcolor(sf::Color::Red);
	  m_alarm_enabled = true;
	  timer = 0;
	}
    }

  timer += dt;
}

void LevelScene::boostUp()
{
  if (m_speed_bar)
    {
      m_speed_bar->increase(m_speed_gain);
    }

  m_asteroids_freq += m_boost_freq;

  m_asteroids_speed *= m_boost_speed;
  m_lightfields_speed *= m_boost_speed;
  m_boost_count++;
}

void LevelScene::boostDown()
{  
  if (m_speed_bar)
    {
      m_speed_bar->decrease(m_speed_gain);

      if (m_speed_bar->value() <= 0)
	{	  
	  m_speed_bar->value(0.0f);
	}
    }

  if (m_life_bar)
    {
      m_life_bar->decrease(0.10);
       
      if (m_life_bar->value() <= 0)
	{
	  gameOver();
	  return;
	}
    }

  if (m_boost_count - 1 > 0 )
    {
      m_asteroids_freq -= m_boost_freq;
  
      if (m_asteroids_speed > 0)
	{
	  m_asteroids_speed /= m_boost_speed;
	  m_lightfields_speed /= m_boost_speed;
	}
      m_boost_count--;
    }
}

void LevelScene::display(sf::RenderWindow &w)
{
  sf::View old_view = w.getView();
  
  displayGame(w);
  w.setView(old_view);
  
  displayUILife(w);
  w.setView(old_view);

  displayUISpeed(w);
  w.setView(old_view);
}

void LevelScene::displayGame(sf::RenderWindow& w)
{
  sf::View game_view = w.getView();
  game_view.setViewport(sf::FloatRect {m_ui_xratio, 0.0, 1 - 2*m_ui_xratio, 1.0});
  w.setView(game_view);
  
  w.draw(m_bg);
  
  m_sky->display(w);

  if (!m_player_explodes)
    {
      m_player->display(w);
    }
  
  for (auto& lightfield : m_lightfields)
    {
      lightfield->display(w);
    }

  for (auto& asteroid : m_asteroids)
    {
      asteroid->display(w);
    }

  m_particules->display(w);

  if (m_alarm_enabled)
    {
      w.draw(m_alarm_feedback);
    }
}

void LevelScene::displayUILife(sf::RenderWindow& w)
{
  sf::View ui_view = w.getView();
  ui_view.setViewport(sf::FloatRect {0.0, 0.0, m_ui_xratio, 1.0});
  w.setView(ui_view);

  if (!m_life_bar)
    {
      m_life_bar = std::make_unique<ProgressBar>(sf::FloatRect{
	  0,
	    0,
	    static_cast<float>(w.getSize().x),
	    static_cast<float>(w.getSize().y)});
      
      m_life_bar->value(1.0);

      m_life_bar->bgColor(sf::Color::Black);
      m_life_bar->valueColor(sf::Color::Red);
    }
  
  m_life_bar->display(w);
}

void LevelScene::displayUISpeed(sf::RenderWindow& w)
{
  sf::View ui_view = w.getView();
  ui_view.setViewport(sf::FloatRect {1 - m_ui_xratio, 0.0, m_ui_xratio, 1.0});
  w.setView(ui_view);

  if (!m_speed_bar)
    {
      m_speed_bar = std::make_unique<ProgressBar>(sf::FloatRect{
	  0,
	    0,
	    static_cast<float>(w.getSize().x),
	    static_cast<float>(w.getSize().y)});
      
      m_speed_bar->bgColor(sf::Color::Blue);
      m_speed_bar->valueColor(sf::Color::Cyan);
    }
  
  m_speed_bar->display(w);
}

void LevelScene::gameOver()
{
  m_state = LevelState::GameOver;
}

void LevelScene::gameComplete()
{
  m_state = LevelState::GameComplete;
}

LevelScene::~LevelScene()
{
  
}
