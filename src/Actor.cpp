#include <Actor.hpp>

Actor::Actor()
{
  
}

void Actor::update(float dt)
{
  m_pos += m_vel * dt;
}

void Actor::display(sf::RenderWindow &w)
{
  w.draw(m_shape);
}


Actor::~Actor()
{
  
}
