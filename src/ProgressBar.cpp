#include <cassert>
#include <ProgressBar.hpp>

ProgressBar::ProgressBar(sf::FloatRect rect)
{
  m_value = 0.0;
  sf::Vector2f pos = {rect.left, rect.top};
  sf::Vector2f sz = {rect.width, rect.height};

  m_bg_shape.setPosition(pos);
  m_bg_shape.setSize(sz);
  m_bg_shape.setFillColor(sf::Color::White);

  m_value_shape.setPosition(pos);
  m_value_shape.setSize(sz);
  m_value_shape.setFillColor(sf::Color::Black);
}

void ProgressBar::update(float dt)
{
  float height = m_bg_shape.getSize().y * m_value;

  m_value_shape.setSize({m_value_shape.getSize().x,
	height});

  float sz_yoffset = (m_bg_shape.getSize().y
  		       - m_value_shape.getSize().y);

  m_value_shape.setPosition({m_value_shape.getPosition().x,
  	m_bg_shape.getPosition().y + sz_yoffset});
}

void ProgressBar::display(sf::RenderWindow& w)
{
  w.draw(m_bg_shape);
  w.draw(m_value_shape);
}

void ProgressBar::value(float value)
{
  assert(value >= 0);
  assert(value <= 1);

  m_value = value;
}

void ProgressBar::increase(float delta)
{
  if (m_value + delta <= 1)
    {
      m_value += delta;
    }
  else
    {
      m_value = 1.0f;
    }
}

void ProgressBar::decrease(float delta)
{
  if (m_value - delta >= 0)
    {
      m_value -= delta;
    }
  else
    {
      m_value = 0.0f;
    }
}

ProgressBar::~ProgressBar()
{
  
}
