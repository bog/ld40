#include <chrono>
#include <random>
#include <Sky.hpp>

Sky::Sky()
  : m_speed (256)
{
  m_array = sf::VertexArray(sf::PrimitiveType::Points, static_cast<int>(STARS_COUNT)*4);

  sf::Vector2f winsz = {
    static_cast<float>( Core::instance()->window().getSize().x ),
    static_cast<float>( Core::instance()->window().getSize().y )
  };
  
  std::mt19937 randpos (std::chrono::steady_clock::now().time_since_epoch().count());

  std::uniform_real_distribution<float> urdx(0.0f, winsz.x);
  std::uniform_real_distribution<float> urdy(0.0f, winsz.y);
  std::uniform_int_distribution<int> uidc(0, 255);
  
  for (size_t i=0; i<STARS_COUNT*4; i+=4)
    {
      sf::Vector2f v (urdx(randpos), urdy(randpos));
      sf::Vector2f v1 = {v.x+1, v.y};
      sf::Vector2f v2 = {v.x+1, v.y+1};
      sf::Vector2f v3 = {v.x, v.y+1};
      
      m_array[i] = sf::Vertex(v, sf::Color(uidc(randpos), uidc(randpos), uidc(randpos)));
      m_array[i+1] = sf::Vertex(v1, sf::Color(uidc(randpos), uidc(randpos), uidc(randpos)));
      m_array[i+2] = sf::Vertex(v2, sf::Color(uidc(randpos), uidc(randpos), uidc(randpos)));
      m_array[i+3] = sf::Vertex(v3, sf::Color(uidc(randpos), uidc(randpos), uidc(randpos)));
    }
}

void Sky::update(float dt)
{
  sf::Vector2f winsz = {
    static_cast<float>( Core::instance()->window().getSize().x ),
    static_cast<float>( Core::instance()->window().getSize().y )
  };
  
  for (size_t i=0; i<STARS_COUNT*4; i++)
    {      
      m_array[i].position.y += m_speed * dt;

      if( m_array[i].position.y > winsz.y )
	{
	  m_array[i].position.y = 0;
	}
    }
}

void Sky::display(sf::RenderWindow& w)
{
  w.draw(m_array);
}


Sky::~Sky()
{
  
}
