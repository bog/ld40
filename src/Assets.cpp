#include <Assets.hpp>

Assets::Assets()
{
  
}

void Assets::loadTexture(std::string name, std::string path)
{
  auto itr = m_textures.find(name);

  if (itr != m_textures.end() )
    {
      std::cerr<< name << " already exists" <<std::endl;
      exit(-1);
    }
  
  std::unique_ptr<sf::Texture> texture = std::make_unique<sf::Texture>();
  
  if( !texture->loadFromFile(path) )
    {
      std::cerr<< "error loading " << name <<std::endl;
      exit(-1);
    }

  m_textures.insert(std::pair<std::string, std::unique_ptr<sf::Texture>>(name, std::move(texture)));
}

sf::Texture& Assets::texture(std::string name)
{
  auto itr = m_textures.find(name);

  if ( itr == m_textures.end() )
    {
      std::cerr<< name << " doesn't exists" <<std::endl;
      exit(-1);
    }

  return *m_textures[name].get();
}

void Assets::loadMusic(std::string name, std::string path)
{
  auto itr = m_musics.find(name);

  if ( itr != m_musics.end() )
    {
      std::cerr<< name << " already exists" <<std::endl;
      exit(-1);
    }
  
  std::unique_ptr<sf::Music> music = std::make_unique<sf::Music>();
  
  if( !music->openFromFile(path) )
    {
      std::cerr<< "error loading " << name <<std::endl;
      exit(-1);
    }
  
  music->setLoop(true);
  
  m_musics.insert(std::pair<std::string, std::unique_ptr<sf::Music>>(name, std::move(music)));
}

sf::Music& Assets::music(std::string name)
{
    auto itr = m_musics.find(name);

    if (itr == m_musics.end() )
      {
	std::cerr<< name << " doesn't exists" <<std::endl;
	exit(-1);
      }
    
    return *m_musics[name].get();
}

void Assets::loadSBuffer(std::string name, std::string path)
{
  auto itr = m_sbuffers.find(name);

  if (itr != m_sbuffers.end() )
    {
      std::cerr<< name << " already exists" <<std::endl;
      exit(-1);
    }
  
  std::unique_ptr<sf::SoundBuffer> sb = std::make_unique<sf::SoundBuffer>();
  
  if( !sb->loadFromFile(path) )
    {
      std::cerr<< "error loading " << name <<std::endl;
      exit(-1);
    }
  
  m_sbuffers.insert(std::pair<std::string, std::unique_ptr<sf::SoundBuffer>>(name, std::move(sb)));
}

sf::SoundBuffer& Assets::sbuffer(std::string name)
{
  auto itr = m_sbuffers.find(name);

  if (itr == m_sbuffers.end() )
    {
      std::cerr<< name << " doesn't exists" <<std::endl;
      exit(-1);
    }
  
  return *m_sbuffers[name].get();
}



Assets::~Assets()
{
  
}
