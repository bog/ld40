#include <BgScene.hpp>

BgScene::BgScene(std::string texture_name)
{
  sf::Vector2f winsz = {
    static_cast<float>(Core::instance()->window().getSize().x),
    static_cast<float>(Core::instance()->window().getSize().y)
  };

  m_bg.setSize(winsz);
  m_bg.setTexture(&Core::instance()->assets().texture(texture_name));
}

void BgScene::update(float dt)
{
  static float timer = 0;
  static const float delay = 0.2;

  if ( sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Return)
       && timer >= delay )
    {
      timer = 0;
      onNext();
      return;
    }

  if ( sf::Keyboard::isKeyPressed(sf::Keyboard::Key::H)
       && timer >= delay )
    {
      timer = 0;
      onHelp();
      return;
    }

  if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Escape)
       && timer >= delay)
    {
      timer = 0;
      onPrevious();
      return;
    }

  timer += dt;
}

void BgScene::display(sf::RenderWindow& w)
{
  w.draw(m_bg);
}


BgScene::~BgScene()
{
  
}
