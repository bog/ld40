#include <Asteroid.hpp>

Asteroid::Asteroid(float xpos)
  : SpaceObject()
{
  m_shape.setSize({AsteroidSize, AsteroidSize});
  m_shape.setFillColor(sf::Color::White);
  m_shape.setTexture(&Core::instance()->assets().texture("asteroids"));
  
  m_speed = AsteroidSpeed;
  m_dir = {0.0f, -1.0, 0.0};
  m_pos = {xpos, - m_shape.getSize().y, 0.0f};
  m_vel = {0.0f, m_speed, 0.0f};
}

Asteroid::~Asteroid()
{
  
}
