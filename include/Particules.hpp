#ifndef PARTICULES_HPP
#define PARTICULES_HPP
#include <iostream>
#include <vector>
#include <Scene.hpp>

constexpr size_t PARTICULES_MAX = 256;

struct Particule
{
  glm::vec3 pos;
  glm::vec3 dir;
  float speed;
  float life;
  bool alive = true;
};
  
class Particules : public Scene
{
public:
  Particules();
  
  void update(float dt) override;
  void display(sf::RenderWindow& w) override;

  void fire(size_t size,
	    sf::Color color,
	    float rad,
	    glm::vec3 origin,
	    glm::vec3 dir,
	    float spread,
	    float life,
	    float speed);

  bool isClear() const { return m_size == 0; }
  
  virtual ~Particules();
  
protected:
  size_t m_size;
  sf::RectangleShape m_shape;
  Particule m_pool[PARTICULES_MAX];
  
private:
  Particules( Particules const& particules ) = delete;
  Particules& operator=( Particules const& particules ) = delete;
};

#endif
