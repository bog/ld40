#ifndef ACTOR_HPP
#define ACTOR_HPP
#include <iostream>
#include <Scene.hpp>

class Actor : public Scene
{
public:
  Actor();

  void update(float dt) override;
  void display(sf::RenderWindow &w) override;

  virtual sf::FloatRect hitbox()
  {
    return sf::FloatRect {m_pos.x,
	m_pos.y,
	m_shape.getSize().x,
	m_shape.getSize().y};
  }

  glm::vec3 pos() const { return m_pos; }
  glm::vec3 dir() const { return m_dir; }
  glm::vec3 vel() const { return m_vel; }
  
  virtual ~Actor();
  
protected:
  sf::RectangleShape m_shape;  
  glm::vec3 m_pos;
  glm::vec3 m_vel;  
  glm::vec3 m_dir;

private:
  Actor( Actor const& actor ) = delete;
  Actor& operator=( Actor const& actor ) = delete;
};

#endif
