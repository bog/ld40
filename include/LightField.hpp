#ifndef LIGHTFIELD_HPP
#define LIGHTFIELD_HPP
#include <iostream>
#include <SpaceObject.hpp>

constexpr float LightfieldHeight = 64;
constexpr float LightfieldWidth = 128;

class LightField : public SpaceObject
{
public:
  LightField(float xpos);
  virtual ~LightField();
  
protected:
  
private:
  LightField( LightField const& lightfield ) = delete;
  LightField& operator=( LightField const& lightfield ) = delete;
};

#endif
