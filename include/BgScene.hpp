#ifndef BGSCENE_HPP
#define BGSCENE_HPP
#include <iostream>
#include <Scene.hpp>
#include <LevelScene.hpp>

class BgScene : public Scene
{
public:
  BgScene(std::string texture_name);
  
  virtual void update(float dt) override;
  virtual void display(sf::RenderWindow& window) override;

  virtual void onPrevious() {}
  virtual void onNext() {}
  virtual void onHelp() {}
  
  virtual ~BgScene();
  
protected:
  sf::RectangleShape m_bg;
  
private:
  BgScene( BgScene const& bgscene ) = delete;
  BgScene& operator=( BgScene const& bgscene ) = delete;
};

struct HelpScene : public BgScene
{  
  HelpScene() : BgScene("help_scene")
  {
    Core::instance()->playMusic("help_music", 30.0f);
  }
  
  void onPrevious() override { Core::instance()->scene(); }
  void onNext() override { Core::instance()->scene(new LevelScene()); }
};

struct MenuScene : public BgScene
{  
  MenuScene() : BgScene("menu_scene")
  {
    Core::instance()->playMusic("menu_music");
  }

  void onNext() override { Core::instance()->scene(new LevelScene()); }
  void onPrevious() override { Core::instance()->quit(); }
  void onHelp() override { Core::instance()->scene(new HelpScene()); }
};

struct GameOverScene : public BgScene
{  
  GameOverScene() : BgScene("gameover_scene")
  {
    Core::instance()->playMusic("gameover_music");
  }
  
  void onPrevious() override { Core::instance()->scene(); }
  void onNext() override { Core::instance()->scene(new LevelScene()); }
};

struct GameCompleteScene : public BgScene
{  
  GameCompleteScene() : BgScene("gamecomplete_scene")
  {
    Core::instance()->playMusic("gamecomplete_music", 50.0);
  }
  
  void onPrevious() override { Core::instance()->scene(); }
  void onNext() override { Core::instance()->scene(new LevelScene()); }
};

#endif
