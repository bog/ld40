#ifndef ASSETS_HPP
#define ASSETS_HPP
#include <iostream>
#include <memory>
#include <unordered_map>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>

#define ASSETS std::string("../assets/")

class Assets
{
public:
  Assets();
  void loadTexture(std::string name, std::string path);
  sf::Texture& texture(std::string name);

  void loadMusic(std::string name, std::string path);
  sf::Music& music(std::string name);

  void loadSBuffer(std::string name, std::string path);
  sf::SoundBuffer& sbuffer(std::string name);
  
  virtual ~Assets();
  
protected:
  std::unordered_map<std::string, std::unique_ptr<sf::Texture>> m_textures;
  std::unordered_map<std::string, std::unique_ptr<sf::Music>> m_musics;
  std::unordered_map<std::string, std::unique_ptr<sf::SoundBuffer>> m_sbuffers;
  
private:
  Assets( Assets const& assets ) = delete;
  Assets& operator=( Assets const& assets ) = delete;
};

#endif
