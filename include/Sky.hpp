#ifndef SKY_HPP
#define SKY_HPP
#include <iostream>
#include <Scene.hpp>

constexpr size_t STARS_COUNT = 256;

class Sky : public Scene
{
public:
  Sky();
  void update(float dt) override;
  void display(sf::RenderWindow& w) override;

  void speed(float sp) { m_speed = sp; }
  float speed() const { return m_speed; }
  
  virtual ~Sky();
  
protected:
  sf::VertexArray m_array;
  float m_speed;
private:
  Sky( Sky const& sky ) = delete;
  Sky& operator=( Sky const& sky ) = delete;
};

#endif
