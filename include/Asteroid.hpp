#ifndef ASTEROID_HPP
#define ASTEROID_HPP
#include <iostream>
#include <SpaceObject.hpp>

constexpr float AsteroidSize = 32;
constexpr float AsteroidSpeed = 512;

class Asteroid : public SpaceObject
{
public:
  Asteroid(float xpos);    
  virtual ~Asteroid();
  
protected:
  
private:
  Asteroid( Asteroid const& asteroid ) = delete;
  Asteroid& operator=( Asteroid const& asteroid ) = delete;
};

#endif
