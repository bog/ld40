#ifndef PLAYER_HPP
#define PLAYER_HPP
#include <iostream>
#include <memory>
#include <Actor.hpp>

constexpr float PlayerWidth = 32;
constexpr float PlayerHeight = 64;

class Particules;

class Player : public Actor
{
public:
  Player();

  void display(sf::RenderWindow& w) override;
  void update(float dt) override;
  
  void pcolor(sf::Color c) { m_pcolor = c; }
  bool escape () const { return m_escape; }
  void escape (bool esc) { m_escape = esc; }
  
  virtual ~Player();
  
protected:
  glm::vec3 m_acc;
  float m_friction;
  std::unique_ptr<Particules> m_particules;
  sf::Color m_pcolor;
  bool m_escape;
  
private:
  Player( Player const& player ) = delete;
  Player& operator=( Player const& player ) = delete;
};

#endif
