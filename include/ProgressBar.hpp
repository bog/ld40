#ifndef PROGRESSBAR_HPP
#define PROGRESSBAR_HPP
#include <iostream>
#include <Scene.hpp>

class ProgressBar : public Scene
{
public:
  ProgressBar(sf::FloatRect rect);
  void update(float dt) override;
  void display(sf::RenderWindow& w) override;

  float value() const { return m_value; }
  void value(float value);
  void increase(float delta);
  void decrease(float delta);

  void bgColor(sf::Color bg) { m_bg_shape.setFillColor(bg); }
  void valueColor(sf::Color bg) { m_value_shape.setFillColor(bg); }
  
  virtual ~ProgressBar();
  
protected:
  sf::RectangleShape m_bg_shape;
  sf::RectangleShape m_value_shape;
  float m_value;
  
private:
  ProgressBar( ProgressBar const& progressbar ) = delete;
  ProgressBar& operator=( ProgressBar const& progressbar ) = delete;
};

#endif
