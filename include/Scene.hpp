#ifndef SCENE_HPP
#define SCENE_HPP
#include <iostream>
#include <glm/glm.hpp>
#include <SFML/Graphics.hpp>
#include <Core.hpp>

class Scene
{
public:
  Scene() {}
  
  virtual void update(float dt) {};
  virtual void display(sf::RenderWindow& window) {};
  
  virtual ~Scene() {}
  
protected:
  
private:
  Scene( Scene const& scene ) = delete;
  Scene& operator=( Scene const& scene ) = delete;
};

#endif
