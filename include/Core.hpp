#ifndef CORE_HPP
#define CORE_HPP
#include <iostream>
#include <memory>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
// Services
#include <Assets.hpp>
// Scenes
class Scene;
//

constexpr size_t SOUND_LIMIT = 32;

class Core
{
public:    
  void update(float dt);
  void display(sf::RenderWindow& window);
  void run();

  // Singleton
  static Core* instance()
  {
    if (!m_instance)
      {
	m_instance = new Core();

	sf::VideoMode mode = sf::VideoMode::getDesktopMode();
	mode.width *= 0.8;
	mode.height *= 0.8;

	m_instance->m_window = new sf::RenderWindow (sf::VideoMode(mode),
						     "Hello world",
						     sf::Style::Close);
      }
    
    return m_instance;
  }
  
  // Services
  sf::RenderWindow& window() { return *m_window; }
  Assets& assets() { return *m_assets.get(); }
  void quit() { stopMusic(); m_window->close(); }
  void playMusic(std::string name);
  void playMusic(std::string name, float volume);
  void stopMusic();
  void playSound(std::string name);
  void scene();
  void scene(Scene* s);
  virtual ~Core();
  
protected:
  // Singleton
  Core();
  static Core* m_instance;
  
  
  // Services
  sf::RenderWindow* m_window;
  std::unique_ptr<Assets> m_assets;
  sf::Music* m_music;
  sf::Sound m_sound[SOUND_LIMIT];
  size_t m_sound_index;
  
  // Scenes
  std::unique_ptr<Scene> m_scene;
  //
  
private:
  Core( Core const& core ) = delete;
  Core& operator=( Core const& core ) = delete;
};

#endif
