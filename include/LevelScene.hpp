#ifndef LEVELSCENE_HPP
#define LEVELSCENE_HPP
#include <iostream>
#include <vector>
#include <memory>
#include <Scene.hpp>

class Player;
class Asteroid;
class LightField;
class ProgressBar;
class Sky;
class Particules;

enum class LevelState {
  Playing,
    GameOver,
    GameComplete
};

class LevelScene : public Scene
{
public:
  LevelScene();
  
  void update(float dt) override;
  void display(sf::RenderWindow &w) override;
  
  virtual ~LevelScene();
  
protected:
  LevelState m_state;
  std::unique_ptr<Player> m_player;
  std::vector<std::unique_ptr<Asteroid>> m_asteroids;
  std::vector<std::unique_ptr<LightField>> m_lightfields;
  std::unique_ptr<Sky> m_sky;
  sf::RectangleShape m_bg;
  sf::RectangleShape m_alarm_feedback;
  bool m_alarm_enabled;
  std::unique_ptr<Particules> m_particules;
  bool m_player_explodes;
  float const m_speed_gain;
  sf::Music* m_music;
  
  // UI
  float m_ui_xratio;
  std::unique_ptr<ProgressBar> m_life_bar;
  std::unique_ptr<ProgressBar> m_speed_bar;
  
  float m_asteroids_freq;
  float m_lightfields_freq;
  float m_boost_freq;
  float m_asteroids_speed;
  float m_lightfields_speed;  
  float m_boost_speed;
  int m_boost_count;
  
  // Update
  void updateAnimation(float dt);
  void updateAnimationGameOver(float dt);
  void updateAnimationGameComplete(float dt);
  void updateAsteroids(float dt);
  void updateLightfields(float dt);
  void updateAsteroidsGeneration(float dt);
  void updateLightfieldsGeneration(float dt);
  void updateUI(float dt);
  void updateAlarm(float dt);
  
  // Boost
  void boostUp();
  void boostDown();
  
  // Display
  void displayGame(sf::RenderWindow& w);
  void displayUILife(sf::RenderWindow& w);
  void displayUISpeed(sf::RenderWindow& w);
  
  // Victory and defait
  void gameOver();
  void gameComplete();
  
private:
  LevelScene( LevelScene const& levelscene ) = delete;
  LevelScene& operator=( LevelScene const& levelscene ) = delete;
};

#endif
