#ifndef SPACE_OBJECT_HPP
#define SPACE_OBJECT_HPP
#include <iostream>
#include <Actor.hpp>

class SpaceObject : public Actor
{
public:
  SpaceObject();
  
  void update(float dt) override;

  bool falling() const { return m_falling; }
  void falling(bool f) { m_falling = f; }

  float speed() const { return m_speed; }
  void speed(float s) { m_speed = s; }
  
  virtual ~SpaceObject();
  
protected:
  bool m_falling;
  float m_speed;
  
private:
  SpaceObject( SpaceObject const& spaceobject ) = delete;
  SpaceObject& operator=( SpaceObject const& spaceobject ) = delete;
};

#endif
